import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  isLoading = false;
  isLogin = true;

  constructor(
    private loadingController: LoadingController,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  onLogin() {
    this.authService.login();
    this.isLoading = true;
    this.loadingController.create({
      message: 'กำลังเข้าสู่ระบบ...',
      keyboardClose: true
    })
    .then(loadingBox => {
      loadingBox.present();
      setTimeout(()=> {
        loadingBox.dismiss();
        this.router.navigateByUrl('/restaurant/tabs/find');
      },2000);
      

    });
  }

}

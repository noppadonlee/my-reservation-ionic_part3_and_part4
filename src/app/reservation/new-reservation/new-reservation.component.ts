import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-new-reservation',
  templateUrl: './new-reservation.component.html',
  styleUrls: ['./new-reservation.component.scss'],
})
export class NewReservationComponent implements OnInit {

  constructor(private modalControlle: ModalController) { }

  ngOnInit() {}

  onCancel() {
    this.modalControlle.dismiss(null, 'cancel');
  }

}

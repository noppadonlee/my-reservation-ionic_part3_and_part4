import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NewReservationComponent } from 'src/app/reservation/new-reservation/new-reservation.component';

@Component({
  selector: 'app-restaurant-detail',
  templateUrl: './restaurant-detail.page.html',
  styleUrls: ['./restaurant-detail.page.scss'],
})
export class RestaurantDetailPage implements OnInit {

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {
  }

  onReservation() {
    this.modalController.create({
      component: NewReservationComponent
    })
    .then( modalElement => {
      modalElement.present();
    });
  }

}

export class Restaurant {
    constructor(
        public id: string,
        public name: string,
        public address: string,
        public imageUrl: string,
        public latitude: string,
        public longitude: string
        
    ) {}
}